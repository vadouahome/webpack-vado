const { basename } = require("path");
const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const OptimizeCssAssetsWebpackPlugin = require("optimize-css-assets-webpack-plugin");
const ImageminPlugin = require("imagemin-webpack");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");

/*визначаємо продакшин та девелопмент*/
const isDev = process.env.NODE_ENV === "development";
const isProd = !isDev;

/**функція оптимізації*/
/**const optimization = () => {
  const configObj = {
    splitChunks: {
      chunks: "all",
    },
  };

  if (isProd) {
    configObj.minimizer = [new OptimizeCssAssetsWebpackPlugin()];
  }

  return configObj;
};*/

/**функція розподілення плагінів для продакшин та девелопмент*/
const plugins = () => {
  const basePlugins = [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: "./index.html",
      template: "./html/main.html",
    }),
    new MiniCssExtractPlugin({
      filename: "./css/[name].css",
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, "src/assets"),
          to: path.resolve(
            __dirname,
            "#" + basename(__dirname + " create by Vado") + "/assets"
          ),
        },
      ],
    }),
    /**new BrowserSyncPlugin({
      host: "localhost",
      port: 3000,
      notify: false,
      server: {
        baseDir: [
          path.resolve(
            __dirname,
            "#" + basename(__dirname + " create by Vado")
          ),
        ],
      },
    }),*/
  ];
  if (isProd) {
    basePlugins.push(
      new ImageminPlugin({
        bail: false,
        cache: true,
        imageminOptions: {
          plugins: [
            ["gifsicle", { interlaced: true }],
            ["jpegtran", { progressive: true }],
            ["optipng", { optimizationLevel: 5 }],
            [
              "svgo",
              {
                plugins: [
                  {
                    removeViewBox: false,
                  },
                ],
              },
            ],
          ],
        },
      })
    );
  }

  return basePlugins;
};

module.exports = {
  /**вхідна та вихідна точка*/
  context: path.resolve(__dirname, "src"),
  mode: "development",
  entry: "./index.js",
  output: {
    filename: "./js/main.js",
    path: path.resolve(
      __dirname,
      "#" + basename(__dirname + " create by Vado")
    ),
    publicPath: "",
  },

  /**оптимізація*/
  /**optimization: optimization(),*/

  /**плагіни та їх налаштування*/
  plugins: plugins(),

  /**визначаємо початкові шляхи написання*/
  devtool: isProd ? false : "source-map",

  /**девсервер*/
  devServer: {
    historyApiFallback: true,
    contentBase: path.resolve(
      __dirname,
      "#" + basename(__dirname + " create by Vado")
    ),
    open: true,
    compress: true,
    hot: true,
    port: 3000,
  },

  /**типи файлів та їх обробка*/
  module: {
    rules: [
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: {
              minimize: {
                removeComments: isProd,
                collapseWhitespace: isProd,
              },
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: (resourcePath, context) => {
                return path.relative(path.dirname(resourcePath), context) + "/";
              },
            },
          },
          "css-loader",
        ],
      },
      {
        test: /\.s[ac]ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: (resourcePath, context) => {
                return path.relative(path.dirname(resourcePath), context) + "/";
              },
            },
          },
          "css-loader",
          "sass-loader",
        ],
      },
      {
        test: /\.pcss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: (resourcePath, context) => {
                return path.relative(path.dirname(resourcePath), context) + "/";
              },
            },
          },
          "css-loader",
          {
            loader: "postcss-loader",
            options: {
              plugins: (loader) => [
                require("postcss-import")(),
                require("postcss-url")(),
                require("postcss-mixins")(),
                require("postcss-advanced-variables")(),
                require("postcss-extend-rule")(),
                require("postcss-atroot")(),
                require("postcss-preset-env")({
                  browsers: [">0.5%"],
                  cascade: false,
                }),
                require("postcss-property-lookup")(),
                require("postcss-nested")(),
                require("cssnano")(isProd),
              ],
            },
          },
        ],
      },
      {
        test: /\.(?:|gif|png|jpg|jpeg|svg|mp4)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path][name].[ext]",
            },
          },
        ],
      },
      {
        test: /\.(?:|ttf|otf|woff|woff2)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path][name].[ext]",
            },
          },
        ],
      },
      {
        test: /\.js$/,
        exclude: /node modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env"],
              plugins: ["@babel/plugin-proposal-class-properties"],
            },
          },
        ],
      },
      /** */
      {
        test: /\.jsx$/,
        exclude: /node modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env", "@babel/preset-react"],
            },
          },
        ],
      },
    ],
  },
};
