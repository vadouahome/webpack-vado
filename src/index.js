/**html*/
import "./html/main.html";

/**js*/
import './components/main.jsx';

/**pcss*/
import "./pcss/fonts.pcss";
import "./pcss/var/vars.pcss";
