document.addEventListener('DOMContentLoaded', () => {
  const form = document.querySelector('.what-i-do');
  const header = document.querySelector('.header');
  const windScroll = document.querySelector('.body-content__content');
  const logoNone = document.querySelector('.header__content__logo');
  const logoNoneA = document.querySelector('.logo');

  const headerFixed = () => {
    let scrollTop = windScroll.scrollTop;

    if (scrollTop >= header.clientHeight * 0.5) {
      header.style.width = `${header.offsetWidth}px`;
      header.classList.add('fixed');
      logoNone.classList.add('fixed');
      logoNoneA.classList.add('fixed');
      windScroll.style.paddingTop = `${header.clientHeight * 1.5}px`;
    } else {
      header.classList.remove('fixed');
      logoNone.classList.remove('fixed');
      logoNoneA.classList.remove('fixed');
      windScroll.style.paddingTop = `0px`;
      header.style.width = `inherit`;
    }
  }

  windScroll.addEventListener('scroll', () => {
    headerFixed();
  });
})