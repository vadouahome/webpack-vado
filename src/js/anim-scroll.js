document.addEventListener("DOMContentLoaded", () => {
  const windScroll = document.querySelector(".body-content__content");
  const scrollItems = document.querySelectorAll(".anime-items");

  const scrollAnimation = () => {
    let animationStart = windScroll.clientHeight * 0.75 + windScroll.scrollTop;

    scrollItems.forEach((el) => {
      let scrollOffset = el.offsetTop;

      if (windScroll.scrollTop < 1 && scrollOffset < animationStart) {
        scrollOffset = scrollOffset + animationStart;
      }

      if (animationStart >= scrollOffset) {
        el.classList.add("anim-active");
      } else {
        if (!el.classList.contains("anim-no-hide")) {
          el.classList.remove("anim-active");
        }
      }
    });
  };

  windScroll.addEventListener("scroll", () => {
    setTimeout(scrollAnimation, 300);
  });
});